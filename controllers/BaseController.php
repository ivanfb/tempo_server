<?php

abstract class BaseController {
	protected $urlvalues;
	protected $action;

	abstract public function handleAction($action);
}

?>
