
<?php

class SearchController extends BaseController {
	
   public function handleAction($action) {
      if ($action == "get_close_services" &&
               isset($_GET['user_id']) && is_string($_GET['user_id']) &&
               isset($_GET['lon']) && is_string($_GET['lon']) &&
               isset($_GET['lat']) && is_string($_GET['lat']) &&
               isset($_GET['kilometers']) && is_string($_GET['kilometers']) &&
               isset($_GET['text_to_search']) && is_string($_GET['text_to_search']) &&
               isset($_GET['category']) && is_string($_GET['category']) &&
               isset($_GET['bool_i_offer']) && is_string($_GET['bool_i_offer']))
      {
         echo DB::getInstance()->getCandidates($_GET['user_id'], $_GET['lon'],
                  $_GET['lat'], $_GET['kilometers'],
                  $_GET['text_to_search'], $_GET['category'], $_GET['bool_i_offer']);
      }
		else if ($action == "get_user_opinions" &&
				isset($_GET['user_id']) && is_string($_GET['user_id'])) 
		{
			echo DB::getInstance()->getOpinions($_GET['user_id']);
		}
		else if ($action == "get_contacts_fresh_info" && 
				isset($_GET['contacts_list']) && is_string($_GET['contacts_list'])) 
		{
			echo DB::getInstance()->getContactsInfo($_GET['contacts_list']);
		}
		else if ($action == "get_firebase_token_id" &&
				isset($_GET['user_id']) && is_string($_GET['user_id'])) 
		{
			echo DB::getInstance()->getFirebaseTokenID($_GET['user_id']);
		}
	}
}

?>