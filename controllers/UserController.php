
<?php

class UserController extends BaseController {
	
   private function encrypt_decrypt($action, $string) {
      $output = false;
      $encrypt_method = "AES-128-ECB";

      $key = base64_decode("AKgBExWb4EPuuSzAvsPfng==");

      if ( $action == 'encrypt' ) {
         $output = openssl_encrypt($string, $encrypt_method, $key, OPENSSL_RAW_DATA);
         $output = base64_encode($output);
      } else if( $action == 'decrypt' ) {
         $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, OPENSSL_RAW_DATA);
      }
      return $output;
   }

	public function handleAction($action) {
	   if ($action == "sign_up" &&
         isset($_POST['user_name']) && is_string($_POST['user_name']) &&
         isset($_POST['email']) && is_string($_POST['email']) &&
			isset($_POST['password']) && is_string($_POST['password']) && 
	      isset($_POST['user_locale']) && is_string($_POST['user_locale']) )
		{
		   $ret = DB::getInstance()->signUpNewUser($_POST['user_name'], $_POST['email'], $_POST['password']);
		   
		   if (strpos($ret, 'OK') === false)
		   { // 'OK' wasnt found
		      echo $ret;       
		   }
		   else
		   {
		      $uid = str_replace("OK", "", $ret); // only the uid
		      $to = $this->encrypt_decrypt('decrypt', $_POST['email']);
		      $subject = $_POST['user_locale'] == "español" ? "T e m p o ~ Confirmación de cuenta" : "T e m p o ~ Email confirmation";
		      $message = $this->createConfirm_Email_In_Html($uid, $_POST['user_name'], $_POST['user_locale']);
		      
		      if ($this->sendEmail($to, $subject, $message))
		      {
		         echo $ret;
		      }
		      else
		      {
		         $this->removeAccount($uid);
		         echo "Invalid 'to' recipient";
		      }
		   }
		}
		elseif ($action == "log_in" &&
		         isset($_POST['email']) && is_string($_POST['email']) &&
		         isset($_POST['password']) && is_string($_POST['password']))
		{
		   echo DB::getInstance()->loginUser($_POST['email'], $_POST['password']);
		}
		elseif ($action == "send_suggestion" &&
		         isset($_POST['suggestion']) && is_string($_POST['suggestion']) &&
		         isset($_POST['uid']) && is_string($_POST['uid']))
		{
		   $this->sendEmail("ivansete@gmail.com", 
		            "Suggestion from " . $_POST['uid'], $_POST['suggestion']);
		   echo "OK";
		}
		elseif ($action == "add_assessment" &&
				isset($_POST['uid']) && is_string($_POST['uid']) &&
				isset($_POST['evaluator_id']) && is_string($_POST['evaluator_id']) &&
				isset($_POST['evaluator_name']) && is_string($_POST['evaluator_name']) &&
				isset($_POST['comment']) && is_string($_POST['comment']) &&
				isset($_POST['mark']) && is_string($_POST['mark']))
		{
			echo DB::getInstance()->addAssessment($_POST['uid'], $_POST['evaluator_id'],
					                              $_POST['evaluator_name'],
					                              $_POST['comment'], $_POST['mark']);
		}
		elseif ($action == "get_user_info" &&
		      isset($_GET['uid']) && is_string($_GET['uid'])) 
		{
		   echo DB::getInstance()->getUserInfo($_GET['uid']);
		}
		elseif ($action == "get_my_contacts_list" &&
		         isset($_GET['uid']) && is_string($_GET['uid']))
		{
		   echo DB::getInstance()->getMyContactsList($_GET['uid']);
		}
		elseif ($action == "send_firebase_notif" &&
		        isset($_POST['uid']) && is_string($_POST['uid']) &&
		        isset($_POST['sender_id']) && is_string($_POST['sender_id']) &&
		        isset($_POST['sender_name']) && is_string($_POST['sender_name']) &&
		        isset($_POST['chat_topic']) && is_string($_POST['chat_topic']))
		{
		   #prep the bundle
		   $msg = array
		   (
		            'body' 	=> 'body',
		            'title'	=> 'Tempo',
		            'sender_id' => $_POST['sender_id'],
		            'sender_name' => $_POST['sender_name'],
		            'chat_topic' => $_POST['chat_topic']
		   );
		   
		   echo $this->sendFirebaseMessage($msg, $_POST['uid']);
		}
		elseif ($action == "send_email_to_reset_password" &&
		       isset($_POST['email']) && is_string($_POST['email']) &&
		       isset($_POST['user_locale']) && is_string($_POST['user_locale']))
		{
		   // the email is sent already encrypted
		   $user_password = DB::getInstance()->getUserPassword($_POST['email']);

		   if (strpos($user_password, 'ERROR') !== false)
		   {
		      echo "ERROR";
		   }
		   else 
		   {
 		      $to = $this->encrypt_decrypt('decrypt', $_POST['email']);
 		      $subject = "Tempo - password recovery";
 		      $reset_code = sprintf("%06d", mt_rand(1, 999999));
 		      $message  = $this->createShowCode_Email_In_Html($reset_code, $_POST['user_locale']);
 		      
		      DB::getInstance()->setCodeResetPassword($_POST['email'], $reset_code);
		      
 		      $this->sendEmail($to, $subject, $message); 		      
   		   echo "OK " . $user_password;
		   }
		}
		else if ($action == "restore_password" &&
		         isset($_POST['email']) && is_string($_POST['email']) &&
		         isset($_POST['code_reset_pass']) && is_string($_POST['code_reset_pass']) &&
		         isset($_POST['new_password']) && is_string($_POST['new_password']))
		{
		   echo DB::getInstance()->restorePassword($_POST['email'], 
		                                           $_POST['code_reset_pass'], 
		                                           $_POST['new_password']);
		}
		else if ($action == "remove_account" &&
		         isset($_POST['uid']) && is_string($_POST['uid']))
		{
		   echo $this->removeAccount($_POST['uid']);
		}
		else if ($action == "remove_user_pic" &&
		         isset($_POST['pic_name']) && is_string($_POST['pic_name']))
		{
		   if(unlink('gs://staging.tempo-213421.appspot.com/users_pics/'
		            . $_POST['pic_name'] . ".jpeg"))
		   {
		      echo "OK";
		   }
		   else {
		      echo "ERROR removing user file(s)";
		   }
		}
		else if ($action == "get_user_other_pic_names" &&
		         isset($_GET['uid']) && is_string($_GET['uid']))
		{
		   $i = 0;
		   $ret = array ('files' => array());
		   $pattern = 'gs://staging.tempo-213421.appspot.com/users_pics/' . $_GET['uid'] . "_*";
		   foreach (glob($pattern) as $file_name) {
		      $ret["files"][$i++] = $file_name;
		   }
		   echo json_encode($ret);
		}
		else if ($action == "confirm_email_account" && // called from 'click' in email
 		         isset($_POST['uid']) && is_string($_POST['uid']))
		{
		   $uid = $_POST['uid'];
		   $ret = DB::getInstance()->setEmailValidated($uid);
		   if ($ret == "OK") {
		      $msg = array
		      (
		               'body' 	=> 'body',
		               'title'	=> 'Tempo',
		               'event_id' => '0' // we will consider email "validation event". See MyMessagingService.java
		      );
		      
		      $this->sendFirebaseMessage($msg, $uid); // TODO, pending to review return value
		      
		      echo $this->createWelcomePage();
		   }
		}
		else if ($action == "update_last_time_access" &&
		         isset($_POST['uid']) && is_string($_POST['uid']))
		{
		   echo DB::getInstance()->updateMyLastAccessDate($_POST['uid']);
		}
		else {
			echo "Error in UserController. No use case was found.";
		}
	}
	
	private function removeAccount($uid) {
	   $result = DB::getInstance()->removeUserAccount($uid);
	   if (strpos($result, "OK") === false)
	   {
	      echo "ERROR removing user from database " . $result;
	   }
	   else
	   {
	      unlink('gs://staging.tempo-213421.appspot.com/users_pics/' . $uid . ".jpeg");
         echo "OK";
	   }
	}
	
	private function sendFirebaseMessage($msg, $to_uid) {
	   // getting firebase token ID
	   $user_token_id = DB::getInstance()->getUserFirebaseTokenId($to_uid);
	   
	   #API access key from Google API's Console
	   define( 'API_ACCESS_KEY', 'AAAAE4hXT7g:APA91bECN7LMzFj9Sfvt6q2euREwRJpsHAjVgjDUXce1oldEJmIulphOu7yzz5aDgdOWG4CBbEc19N8VUi-AowyXgZ9fDNIHuj6NyLIzH24yyxWAJYOY0gDufLsRcIwdwYZ0Zr6i9fP7' );
	   
	   $fields = array
	   (
	            'to'	=> $user_token_id,
	            'data'	=> $msg,
	            "priority" => "high"
	   );
	   
	   $headers = array
	   (
	            'Authorization: key=' . API_ACCESS_KEY,
	            'Content-Type: application/json'
	   );
	   #Send Reponse To FireBase Server
	   $ch = curl_init();
	   curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
	   curl_setopt( $ch,CURLOPT_POST, true );
	   curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
	   curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
	   curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
	   curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
	   $result = curl_exec($ch );
	   curl_close( $ch );
	   #Echo Result Of FireBase Server
	   return $user_token_id . "]XXX[" . $result;
	}
	
	private function createShowCode_Email_In_Html($code, $user_locale) {
	   $message = '<html><body><center>';
	   $message .= '<img src="https://tempo-213421.appspot.com/app_icon.png" alt="app icon">';
	   $message .= '<br>';
	   
	   if ($user_locale == "español")
	   {
	      $message .= '<p style="font-size:16px; color: black;">Usa el siguiente código para reestablecer la contraseña</p>';
	   }
	   else {
	      $message .= '<p style="font-size:16px; color: black;">Use the following code to restore the password</p>';
	   }
	   
	   $message .= '<p style="font-size:20px; color: black;"><strong>' . $code . '</strong></p>';
	   $message .= '<br>';
	   $message .= '</center></body></html>';
	   
	   return $message;
	}
	
	private function createConfirm_Email_In_Html($uid, $user_name, $user_locale) {
	   $message = '<html><body><center>';
      $message .= '<img src="https://tempo-213421.appspot.com/app_icon.png" alt="app icon">';
      $message .= '<br>';
      
      if ($user_locale == "español")
	   {
	      $message .= '<p style="font-size:16px; color: black;">Bienvenido/a a <strong>Tempo</strong> '. $user_name .',</p>';
	      $message .= '<p style="font-size:16px; color: black;">Por favor, valida tu cuenta</p>';
	      $text_to_show = 'Validar';
	   }
	   else {
	      $message .= '<p style="font-size:16px; color: black;">Welcome to <strong>Tempo</strong> '. $user_name .',</p>';
   	   $message .= '<p style="font-size:16px; color: black;">Please, validate your email account</p>';
	      $text_to_show = 'Validate';
	   }
	   $obfuscated_uid = $this->encrypt_decrypt('encrypt', $uid);
	   
	   $message .= '<br>';
	   $message .= '<a style="display: block; width: 115px; height: 25px; background: #00bbff; padding: 10px;
                   text-align: center; border-radius: 5px; color: white; text-decoration: none !important;
                   font-weight: bold; font-size:20px" 
              href="https://tempo-213421.appspot.com/welcome.php?a=' . rawurlencode($obfuscated_uid) .'">'. $text_to_show .'</a>';
      $message .= '<br>';
	   $message .= '</center></body></html>';
	   
	   return $message;
	}
	
	private function sendEmail($to, $subject, $message) {
	   $headers = "";
	   $headers .= "From: T e m p o <tempo-noreply@tempo-213421.appspotmail.com>" . "\r\n";
	   $headers .= "Reply-To: <tempo-noreply@tempo-213421.appspotmail.com>" . "\r\n";
	   $headers .= 'MIME-Version: 1.0' . "\r\n";
	   $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	   
	   return mail($to, $subject, $message, $headers);
	}
	
	private function createWelcomePage() {
	   $page = '<html><body>';
	   $page .= '<br>';
	   $page .= '<center><img src="https://tempo-213421.appspot.com/app_icon.png" alt="app icon"></center>';
	   $page .= '<br>';
	   $page .= '<center><h1>T e m p o</h1></center>';
	   $page .= '<br>';
	   $page .= '<center><h1 style="color:#00bbff;">Cuenta confirmada, gracias :)</h1></center>';
  	   $page .= '</body></html>';
	   
	   return $page;
	}
}

?>