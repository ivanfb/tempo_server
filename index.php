<?php 

require_once 'library/Log.php';
require_once 'library/Config.php';
require_once 'library/Shared.php';
require_once 'library/Main.php';

try {
	Main::run();
}
catch (Exception $e) {
	error_log("Excepcion: " . $e->getMessage() . " " . $e->getFile() . "(" . $e->getLine() . ")");
	error_log(var_export($e->getTraceAsString(), true)); // imprime la pila
}

?>


