<?php

require_once 'inc/apache-log4php-2.3.0/src/main/php/Logger.php';

class Log {
    const logger_name = "logger";
	
	public static function info ($message) {
		Logger::getLogger(Log::logger_name)->info($message);
	}
	
	public static function debug ($message) {
		Logger::getLogger(Log::logger_name)->debug($message);
	}
}

?>