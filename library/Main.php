<?php

foreach (glob("controllers/*.php") as $filename)
{
	require_once $filename;
}

class Main {
	/** Check if environment is development and display errors **/
	private static function setReporting() {
		if (DEBUG == true) {
			error_reporting ( E_ALL );
			ini_set ( 'display_errors', 'On' );
		} else {
			error_reporting ( E_ALL );
			ini_set ( 'display_errors', 'Off' );
			ini_set ( 'log_errors', 'On' );
			ini_set ( 'error_log', ROOT . DS . 'tmp' . DS . 'logs' . DS . 'error.log' );
		}
	}
	
	/** Check for Magic Quotes and remove them **/
	private static function stripSlashesDeep($value) {
		$value = is_array ( $value ) ?
		array_map ( 'stripSlashesDeep', $value ) : stripslashes ( $value );
		return $value;
	}
	
	private static function removeMagicQuotes() {
		if (get_magic_quotes_gpc ()) {
			$_GET = stripSlashesDeep ( $_GET );
			$_POST = stripSlashesDeep ( $_POST );
			$_COOKIE = stripSlashesDeep ( $_COOKIE );
		}
	}
	
	/** Check register globals and remove them */
	private static function unregisterGlobals() {
		if (ini_get ( 'register_globals' )) {
			$array = array (
					'_SESSION',
					'_POST',
					'_GET',
					'_COOKIE',
					'_REQUEST',
					'_SERVER',
					'_ENV',
					'_FILES'
			);
			foreach ( $array as $value ) {
				foreach ( $GLOBALS [$value] as $key => $var ) {
					if ($var === $GLOBALS [$key]) {
						unset ( $GLOBALS [$key] );
					}
				}
			}
		}
	}
	
	public static function run() 
	{
		Main::setReporting ();
		Main::removeMagicQuotes ();
		Main::unregisterGlobals ();
		
		if (isset ($_GET['controller']) && is_string($_GET['controller'])) {
		   $controllerName = $_GET ['controller'] . "Controller";
		   $controller = new $controllerName;
		}
		else if (isset ($_POST['controller']) && is_string($_POST['controller'])) {
		   $controllerName = $_POST ['controller'] . "Controller";
		   $controller = new $controllerName;
		}
		else {
		   echo "controller was not defined";
		   return;
		}

		if (isset ($_GET['action']) && is_string($_GET['action'])) {
		   $controller->handleAction($_GET['action']);
		}
		else if (isset ($_POST['action']) && is_string($_POST['action'])) {
		   $controller->handleAction($_POST['action']);
		}
		else {
			$controller->index();
		}
	}
}

?>