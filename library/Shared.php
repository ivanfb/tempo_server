<?php

/** Autoload any classes that are required */
spl_autoload_register('myAutoloader');
function myAutoloader($className)
{
	if (file_exists ( ROOT . DS . 'library' . DS . $className . '.php' )) 
	{
		require_once (ROOT . DS . 'library' . DS . $className . '.php');
	} 
	else if (file_exists ( ROOT . DS . 'controllers' . DS . $className . '.php' )) 
	{
		require_once (ROOT . DS . 'controllers' . DS . $className . '.php');
	} 
	else if (file_exists ( ROOT . DS . 'modelo' . DS . $className . '.php' )) 
	{
		require_once (ROOT . DS . 'modelo' . DS . $className . '.php');
	} 
	else {
		/* Error Generation Code Here */
	}
}

?>


