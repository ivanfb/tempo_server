<?php
define ( "DEFAULT_INITIAL_WORKER_NAME", "XXXXXX" );
class DB
{
   // inicio singleton
   private static $instancia;

   private function __construct()
   {
      $this->db = new mysqli ( getenv("MYSQL_IP"), getenv("MYSQL_USER"),
                               getenv("MYSQL_PASSWORD"), getenv("MYSQL_DATABASE_NAME"));
      
      if ($this->db->connect_errno > 0)
      {
         die ('Unable to connect to database [' . $this->db->connect_error . ']' );
      }
      $this->db->query ( "SET NAMES 'utf8'" );
      
      /* disable autocommit */
      $this->db->autocommit ( FALSE );
   }

   public static function getInstance()
   {
      if (! self::$instancia instanceof self)
      {
         self::$instancia = new self ();
      }
      return self::$instancia;
   }
   // fin singleton
   private $db;

   public function addUserError($user_id, $error_message)
   {
      // adding the new assessment
      $evaluation_date = date ( 'Y-m-d H:i:s' );
      
      $result = $this->db->query (
               "INSERT INTO ERROR (`UID` ,`DATE` ,`MESSAGE`) " .
               "VALUES ('" . $user_id . "', '" . $evaluation_date . 
               "', '" . $error_message . "')" );
      
      if (! $result)
      {
         return "Error inserting new error " . $this->db->error;
      }
      
      if (! $this->db->commit ())
      {
         return "ERROR committing new error " . $this->db->error;
      }
      
      return "OK";
   }
   
   public function updateUserInfo($user_id, $name, $location, $lon, $lat, $services)
   {
      // updating user metadata
      $result = $this->db->query ( 
               "UPDATE `USER` SET " . 
               "NAME='" . $name . "', " . 
               "LOCATION='" . $location . "', " .
               "LAT=" . $lat . ", " .
               "LON=" . $lon . " " .
               "WHERE UID = " . $user_id );
      if (! $result)
      {
         return "Error " . $this->db->error;
      }
      
      // removing user abilities
      $result = $this->db->query ( 
               "DELETE from SERVICE where UID='" . $user_id . "'" );
      if (! $result)
      {
         return "Error deleting services " . $this->db->error;
      }
      
      // updating user's services
      foreach ( $services as $service )
      {
         $result = $this->db->query ( 
                  "INSERT INTO SERVICE (UID, CATEGORY, IS_A_NEED, DETAILED_DESCR) VALUES ('" . 
                  $user_id . "','" . $service->category . "','" . 
                  $service->is_a_need . "','" . $service->detailed_desc . "')" );
         if (! $result)
         {
            return "Error adding new abilities " . $this->db->error;
         }
      }

      if ($this->db->commit ())
      {
         return "OK";
      }
      else
      {
         return "ERROR in commit (updateUserInfo)";
      }
   }

   public function deleteContacts($user_id, $contacts_array)
   {
      $ids = implode("','", $contacts_array);
      $result = $this->db->query ("DELETE FROM CONTACT WHERE UID = " . $user_id .
               " AND CONTACT_ID IN ('" . $ids . "')");
      if (! $result)
      {
         return "Error deleting contacts " . $this->db->error;
      }
      
      if ($this->db->commit ())
      {
         return "OK";
      }
      else
      {
         return "ERROR deleting contacts in commit " . $this->db->error;
      }
   }
   
   public function updateContactInfo($user_id, $contacts)
   {
      foreach ( $contacts as $contact )
      {
         $result = $this->db->query (
                  "INSERT INTO CONTACT (UID, CONTACT_ID, SERVICE) VALUES (" .
                  $user_id . "," . $contact->{'contact_id'} . ",'" . $contact->{'service'} . "') " .
                  "ON DUPLICATE KEY UPDATE SERVICE = VALUES(SERVICE)" );

         if (! $result)
         {
            return "Error updating contact info " . $this->db->error;
         }
      }
      
      if ($this->db->commit ())
      {
         return "OK";
      }
      else
      {
         return "ERROR adding contacts list " . $this->db->error;
      }
   }
   
   public function updatePicChangeTime($user_id)
   {
      // updating user metadata
      $result = $this->db->query (
               "UPDATE `USER` SET PICS_CHANGE_TIME = " . time() .
               " WHERE UID = " . $user_id );
      if (! $result)
      {
         return "Error " . $this->db->error;
      }
      
      if ($this->db->commit ())
      {
         return "OK";
      }
      else
      {
         return "ERROR in commit (updatePicChangeTime)";
      }
   }

   public function saveFirebaseToken($user_id, $token)
   {
      // updating user firebase token
      $sql = "UPDATE `USER` SET FIREBASE_TOKEN = '" . $token . "' " . 
      "WHERE UID = " . $user_id;
      $result = $this->db->query ( $sql );
      if (! $result)
      {
         return "Error " . $this->db->error . " [" . $sql . "]";
      }
      
      if ($this->db->commit ())
      {
         return "OK";
      } 
      else
      {
         return "ERROR in commit (updateUserInfo)";
      }
   }

   public function getFirebaseTokenID($user_id)
   {
      $sql = "SELECT FIREBASE_TOKEN from USER WHERE UID = " . $user_id;
      $result = $this->db->query ( $sql );
      if (! $result)
      {
         return "Error " . $this->db->error . " [" . $sql . "]";
      }
      
      $row = $result->fetch_assoc ();
      
      if (empty ( $row ['FIREBASE_TOKEN'] ))
      {
         return "No FIREBASE_TOKEN for user " . $user_id;
      }
      mysqli_free_result($result);
      
      return $row ['FIREBASE_TOKEN'];
   }

   /**
    *
    * @param unknown $user_id
    * @param unknown $lon
    * @param unknown $lat
    * @param unknown $kilometers
    * @param unknown $text_to_search
    * @return string
    */
   public function getCandidates($user_id, $lon, $lat, $kilometers, 
            $text_to_search, $category /* short descr */, $bool_i_offer)
   {
      $search_date = date ( 'Y-m-d H:i:s' );
      
      // adding the new entry in SEARCH table
      $result = $this->db->query ( 
               "INSERT INTO SEARCH (`UID`, `CATEGORY`, `TEXT` , `I_OFFER`, `LAT`, `LON`, `DATE`)
                 VALUES (" . $user_id . ", '" . $category . "', '" . $text_to_search .
               "', '" . $bool_i_offer . "', " . $lat . ", " . $lon . 
                         ", '" . $search_date . "')" );
      if ($result)
      {
         $this->db->commit (); // don't do any special validation
      }
//       else
//       {
//          return "Error: " . $this->db->error;
//       }
      
      if ($lon < 0.01 && $lat < 0.01) 
      {
         $kilometers = 5000; // enlarge the default limit if the mobile app couldn't get user's location
      }
      
      // Close candidates
      $query_close_candidates = 
      "st_distance_sphere(point(w.LON, w.LAT), point(" . $lon . ", " . $lat . 
      ")) / 1000 < " . $kilometers . " and w.UID != " . $user_id;
      
      $final_query = "";
      
      // Category
      $category_filter = "";
      if (strcmp($category, "cat_any" /*defined in Android app*/) != 0 )
      {
         $category_filter = " AND s.CATEGORY = '" . $category . "' ";
      }
      
      // "I offer vs "I need"
      $i_offer_i_need_filter = "";
      if (strcmp($bool_i_offer, "-1" /* if -1 we'll return both need and offered */) != 0 )
      {
         $i_offer_i_need_filter = " AND s.IS_A_NEED = '". $bool_i_offer ."' ";
      }
      
      if (empty ( $text_to_search ))
      {
         $final_query = "SELECT w.UID, w.NAME, w.LOCATION, w.LAT, w.LON, " .
            "w.AVERAGE_MARK, w.NUM_VOTES_RX, w.PICS_CHANGE_TIME,
				s.CATEGORY, s.DETAILED_DESCR, s.IS_A_NEED
				FROM USER w LEFT JOIN SERVICE s ON w.uid = s.uid
				WHERE w.name != '" . constant ( 
                  "DEFAULT_INITIAL_WORKER_NAME" ) . "'" . 
                  $category_filter .
                  $i_offer_i_need_filter .
				"AND " . $query_close_candidates;
      } 
      else
      {
         $final_query = "SELECT w.UID, w.NAME, w.LOCATION, w.LAT, w.LON, " .
            "w.AVERAGE_MARK, w.NUM_VOTES_RX, w.PICS_CHANGE_TIME,
				s.CATEGORY, s.DETAILED_DESCR, s.IS_A_NEED, 
                 MATCH(s.DETAILED_DESCR) AGAINST ('" . $text_to_search . "') as score
				FROM USER w LEFT JOIN SERVICE s ON w.uid = s.uid
				WHERE w.name != '" . constant ( 
                  "DEFAULT_INITIAL_WORKER_NAME" ) . "'" . 
                  $category_filter .
                  $i_offer_i_need_filter .
				"AND " . $query_close_candidates . 
				" AND MATCH(ab.DETAILED_DESCR) AGAINST ('" . 
         $text_to_search . "') > 0 order by score desc";
      }
      
      $result = $this->db->query ( $final_query );
      
      if (! $result)
      {
         return "Error: " . $this->db->error;
      }
      
      $i = 0;
      $ret = array ();
      $prev_uid = "-1";
      while ( $row = $result->fetch_assoc () )
      {
         if ($prev_uid != $row ['UID'] && ! empty ( 
                  $row ['NAME'] ) && ! empty ( $row ['CATEGORY'] ))
         {
            $prev_uid = $row ['UID'];
            
            $ret [$i ++] = array (
                                 'uid' => $row ['UID'],
                                 'name' => $row ['NAME'],
                                 'location' => $row ['LOCATION'],
                                 'average_mark' => $row ['AVERAGE_MARK'],
                                 'num_votes' => $row ['NUM_VOTES_RX'],
                                 'services' => array (),
                                 'pics_change_time' => $row ['PICS_CHANGE_TIME']
            );
         }
         
         if ($row ['CATEGORY'] != null)
         {
            array_push ( $ret [$i - 1] ['services'], 
                     array (
                           'category' => $row ['CATEGORY'],
                           'detailed_descr' => $row ['DETAILED_DESCR'], 
                           'is_a_need' => $row ['IS_A_NEED']   
                     ) );
         }
      }

      mysqli_free_result($result);
      
      return Json_encode ( $ret );
   }

   public function getContactsInfo($users_ids)
   {
      $result = $this->db->query ( 
               "SELECT w.UID, w.NAME, w.LOCATION, w.AVERAGE_MARK, w.NUM_VOTES_RX, w.PICS_CHANGE_TIME, " . 
               "s.CATEGORY, " .
               "s.DETAILED_DESCR, " .
               "s.IS_A_NEED " .
               "FROM USER w LEFT JOIN SERVICE s ON w.uid = s.uid " .
               "WHERE w.name != '" . constant ( 
                        "DEFAULT_INITIAL_WORKER_NAME" ) . 
               "' " . "AND w.UID in (" . $users_ids . ")" );
      
      if (! $result)
      {
         return "Error in getContactsInfo: " . $this->db->error;
      }
      
      $i = 0;
      $ret = array ();
      $prev_uid = "-1";
      while ( $row = $result->fetch_assoc () )
      {
         if ($prev_uid != $row ['UID'] && ! empty($row['NAME']) )
         {
            $prev_uid = $row ['UID'];
            
            $ret [$i ++] = array (
                                 'uid' => $row ['UID'],
                                 'name' => $row ['NAME'],
                                 'location' => $row ['LOCATION'],
                                 'average_mark' => $row ['AVERAGE_MARK'],
                                 'num_votes' => $row ['NUM_VOTES_RX'],
                                 'pics_change_time' => $row ['PICS_CHANGE_TIME'],
                                 'services' => array ()
            );
         }
         
         if ($row ['CATEGORY'] != null && ! empty ( $row ['CATEGORY'] ))
         {
            array_push ( $ret [$i - 1] ['services'],
                     array (
                              'category' => $row ['CATEGORY'],
                              'detailed_descr' => $row ['DETAILED_DESCR'],
                              'is_a_need' => $row ['IS_A_NEED']
                     ) );
         }
      }
      
      mysqli_free_result($result);
      
      return Json_encode ( $ret );
   }

   private function getOpinionsAsArray($user_id)
   {
      $ret = array (
                  'received_opinions' => array (),
                  'given_opinions' => array () 
      );
      
      // received opinions
      $result = $this->db->query ( 
               "SELECT u.NAME, a.EVALUATION_DATE, a.COMMENT, a.MARK " . 
               "FROM ASSESSMENT a " .
               "LEFT JOIN USER u ON a.EVALUATOR = u.UID ".
               "where a.UID = '" . $user_id . "'" );
      
      if (! $result)
      {
         return "Error getting opinions for user [" . $user_id . "]: " . $this->db->error;
      }
      
      $i = 0;
      while ( $row = $result->fetch_assoc () )
      {
         $ret ['received_opinions'] [$i ++] = array (
                                                   'evaluator_name' => $row ['NAME'],
                                                   'evaluation_date' => $row ['EVALUATION_DATE'],
                                                   'comment' => $row ['COMMENT'],
                                                   'mark' => $row ['MARK'] 
         );
      }
      
      mysqli_free_result($result);
      // end of received opinions
      
      // given opinions
      $result = $this->db->query ( 
               "SELECT w.NAME, a.EVALUATION_DATE, a.COMMENT, a.MARK " . 
               "FROM USER w LEFT JOIN ASSESSMENT a ON w.uid = a.uid " . 
               "WHERE EVALUATOR = '" . $user_id . "'" );
      
      if (! $result)
      {
         return "Error sent opinions by user [" . $user_id . "]: " . $this->db->error;
      }
      
      $i = 0;
      while ( $row = $result->fetch_assoc () )
      {
         $ret ['given_opinions'] [$i ++] = array (
                                                'opinion_receiver' => $row ['NAME'],
                                                'evaluation_date' => $row ['EVALUATION_DATE'],
                                                'comment' => $row ['COMMENT'],
                                                'mark' => $row ['MARK'] 
         );
      }

      mysqli_free_result($result);
      
      return $ret;
   }

   public function getOpinions($user_id)
   {
      return Json_encode ( 
               $this->getOpinionsAsArray ( $user_id ) );
   }

   // Consider email validated by default
   public function getUserInfo($user_id, $email_validated = "1")
   {
      $query = "SELECT UID, NAME, LOCATION, LAT, LON,
               AVERAGE_MARK, NUM_VOTES_RX, PICS_CHANGE_TIME
					FROM USER WHERE UID = " . $user_id;
      
      $result = $this->db->query ( $query );
      
      if (! $result)
      {
         return "Error: " . $this->db->error;
      }
      
      $prev_worker_id = "-1";
      if ($row = $result->fetch_assoc ())
      {
         $ret = array (
                     'uid' => $row ['UID'],
                     'name' => $row ['NAME'],
                     'location' => $row ['LOCATION'],
                     'lat' => $row ['LAT'],
                     'lon' => $row ['LON'],
                     'average_mark' => $row ['AVERAGE_MARK'],
                     'num_votes' => $row ['NUM_VOTES_RX'],
                     'services' => array(),
                     'opinions' => array (),
                     'pics_change_time' => $row ['PICS_CHANGE_TIME'],
                     'email_validated' => $email_validated
         );
         
         mysqli_free_result($result);
         
         // user's services
         $query = "select CATEGORY, DETAILED_DESCR, IS_A_NEED from SERVICE where UID = " . $user_id;
         
         $result = $this->db->query ( $query );
         
         if (! $result)
         {
            return "Error getting user's abilites: " . $this->db->error;
         }
         
         while ($row = $result->fetch_assoc ())
         {
            array_push ( $ret ['services'], 
                     array (
                           'category' => $row ['CATEGORY'],
                           'detailed_descr' => $row ['DETAILED_DESCR'], 
                           'is_a_need' => $row ['IS_A_NEED']
                     ) );
         } 
         
         mysqli_free_result($result);
         // end getting user's services

         // user's opinions
         array_push ( $ret ['opinions'], $this->getOpinionsAsArray ( $user_id ) );
      } 
      else
      {
         return "ERROR: info not found for user " . $user_id;
      }
      
      return Json_encode ( $ret );
   }

   public function loginUser($_email, $_password)
   {
      $select_query = "SELECT `UID`,`EMAIL_VALIDATED` FROM `CREDENTIALS`
                  WHERE EMAIL = '" . $_email . "' AND ENCRYPTED_PASSW0RD = '" . $_password . "'";
      // asking for the brand new user id
      $result = $this->db->query ($select_query);
      if (! $result)
      {
         return "Error in [". $select_query ."] when looking for UID " . $this->db->error;
      }
      
      $row = $result->fetch_row ();

      mysqli_free_result($result);
      
      $user_id = $row [0];
      $email_validated = $row [1];

      if ($user_id > 0)
      {
         return $this->getUserInfo($user_id, $email_validated);
      }
      else
      {
         return "INVALID_CREDENTIALS";
      }
   }
   
   public function signUpNewUser($_user_name, $_email, $_password)
   {
      // check if the email is already registered
      $result = $this->db->query (
               "SELECT `UID` FROM `CREDENTIALS` WHERE EMAIL = '" . $_email . "'");
      if ($result && $result->fetch_row () [0] > 0)
      {
         return "EMAIL_REPEATED";
      }
      mysqli_free_result($result);
      
      // adding the new entry in credentials table
      $result = $this->db->query ( 
               "INSERT INTO CREDENTIALS (`UID` ,`EMAIL` ,`ENCRYPTED_PASSW0RD`) VALUES (NULL , '" . 
               $_email . "', '" . $_password . "')" );
      
      if (! $result)
      {
         return "Error inserting new user " . $this->db->error;
      }
      
      if (! $this->db->commit ())
      {
         return "ERROR singinUpNew user in first commit" . $this->db->error;
      }
      
      // asking for the brand new user id
      $result = $this->db->query ( 
               "SELECT `UID` FROM `CREDENTIALS`
                  WHERE EMAIL = '" . $_email . "' AND ENCRYPTED_PASSW0RD = '" . $_password . "'" );
      if (! $result)
      {
         return "Error in select after inserting user: " . $this->db->error;
      }
      
      $user_id = $result->fetch_row () [0];
      mysqli_free_result($result);
      
      // adding new naïve entry in USER table
      $result = $this->db->query ( 
               "INSERT INTO `USER` " . 
               "(`UID` ,`NAME` ,`LOCATION`, `AVERAGE_MARK`, `NUM_VOTES_RX`, `LAST_ACCESS_DATE`) " . 
               " VALUES ('" . $user_id . "' , '" . 
                            $_user_name . "', 'location', 0, 0, '". date ( 'Y-m-d H:i:s' ) ."')" );
      
      if (! $result)
      {
         return "Error inserting worker after signing in " . $this->db->error;
      }
      
      if (! $this->db->commit ())
      {
         return "ERROR singingUpUser second commit " . $this->db->error;
      }
      
      return "OK" . $user_id . "OK";
   }

   public function setEmailValidated($user_id)
   {
      $sql = "UPDATE `CREDENTIALS` SET EMAIL_VALIDATED = '1' WHERE UID = " . $user_id;
      $result = $this->db->query ( $sql );
      if (! $result)
      {
         return "Error " . $this->db->error . " [" . $sql . "]";
      }
      
      if ($this->db->commit ())
      {
         return "OK";
      }
      else
      {
         return "ERROR in commit (setEmailValidated)";
      }
   }
   
   public function addAssessment($user_id, $evalutator_id, $evaluator_name, 
            $comment, $mark)
   {
      // adding the new assessment
      $evaluation_date = date ( 'Y-m-d H:i:s' );
      
      $result = $this->db->query ( 
               "INSERT INTO ASSESSMENT (`UID` ,`EVALUATOR` ,`EVALUATOR_NAME`," . 
               "`EVALUATION_DATE`,`COMMENT`,`MARK`) VALUES ('" . $user_id . 
               "', '" . $evalutator_id . "', '" . $evaluator_name . "', '" . 
               $evaluation_date . "','" . $comment . "','" . $mark . "')" );
      
      if (! $result)
      {
         return "Error inserting new assessment " . $this->db->error;
      }
      
      if (! $this->db->commit ())
      {
         return "ERROR committing new assessment" . $this->db->error;
      }
      
      // getting the resulting average
      $result = $this->db->query ( 
               "SELECT AVG(MARK) FROM `ASSESSMENT`
                  WHERE UID = '" . $user_id . "'" );
      if (! $result)
      {
         return "Error getting the average for user [" . $user_id . "]: " . $this->db->error;
      }
      
      $mark_average = $result->fetch_row () [0];
      mysqli_free_result($result);
      
      // storing the new average in USER table
      $result = $this->db->query ( 
               "UPDATE `USER` SET " . "AVERAGE_MARK ='" . $mark_average . "', " . 
               "NUM_VOTES_RX = NUM_VOTES_RX + 1 " . "WHERE UID = '" . $user_id . "'" );
      
      if (! $result)
      {
         return "Error setting mark average in USER table for user [" . $user_id . "]" . $this->db->error;
      }
      
      if (! $this->db->commit ())
      {
         return "ERROR committing new mark average in USER table for user [" . $user_id . "]" . $this->db->error;
      }
      
      return "OK";
   }
   
   public function restorePassword($_email, $_code_reset_pass, $_new_password)
   {
      // updating user metadata
      $result = $this->db->query (
               "UPDATE `CREDENTIALS` SET " .
               "ENCRYPTED_PASSW0RD='" . $_new_password . "' " .
               "WHERE EMAIL = '" . $_email . "' AND ".
               "CODE_RESET_PASSWORD = '". $_code_reset_pass ."' AND " .
               "RESET_PASS_EXPIRATION >= NOW() " );
      if (! $result)
      {
         return "Error " . $this->db->error;
      }
      
      if ($this->db->commit ())
      {
         return "OK";
      }
      else
      {
         return "ERROR in commit (restorePassword)";
      }
   }
   
   public function setCodeResetPassword($_email, $_code_reset_pass)
   {
      $expiration_date = date ( "Y-m-d H:i:s", time() + 86400 );
      
      $result = $this->db->query (
               "UPDATE `CREDENTIALS` SET " .
               "CODE_RESET_PASSWORD='" . $_code_reset_pass . "', " .
               "RESET_PASS_EXPIRATION='" . $expiration_date . "' " .
               "WHERE EMAIL = '" . $_email . "'");
      if (! $result)
      {
         return "Error updating CREDENTIALS, setting reset code " . $this->db->error;
      }
      
      if (! $this->db->commit ())
      {
         return "ERROR committing CREDENTIALS, setting reset code [" . $_email . "]" . $this->db->error;
      }
      
      return "OK";
   }
   
   public function getUserPassword($_email) 
   {
      $select_query = "SELECT `ENCRYPTED_PASSW0RD` FROM `CREDENTIALS`
                  WHERE EMAIL = '" . $_email . "'";
      // asking for the brand new user id
      $result = $this->db->query ($select_query);
      if (! $result)
      {
         return "ERROR in [". $select_query ."] when looking for UID " . $this->db->error;
      }
      
      if ($result->num_rows == 1)
      {
         $ret = $result->fetch_row () [0];
         mysqli_free_result($result);
         return $ret;
      }
      else
      {
         return "ERROR WRONG_NUM_ROWS " . $result->num_rows;
      }
   }
   
   public function removeUserAccount($user_id)
   {
      $result = $this->db->query ("DELETE FROM CREDENTIALS WHERE UID = " . $user_id);
      if (! $result)
      {
         return "Error deleting credentials " . $this->db->error;
      }
      else
      {
         if (! $this->db->commit ())
         {
            return "ERROR when committing the account deletion";
         }
      }
      return "OK";
   }

   public function getUserFirebaseTokenId($user_id)
   {
      $select_query = "SELECT `FIREBASE_TOKEN` FROM `USER`
                  WHERE UID = '" . $user_id . "'";
      // asking for the brand new user id
      $result = $this->db->query ($select_query);
      if (! $result)
      {
         return "ERROR in [". $select_query ."] when looking for UID " . $this->db->error;
      }
      
      if ($result->num_rows == 1)
      {
         $ret = $result->fetch_row () [0];
         
         mysqli_free_result($result);
         
         return $ret;
      }
      else
      {
         return "ERROR_WRONG_NUM_ROWS " . $result->num_rows;
      }
   }
   
   // Returns the list of contacts. Needed when logging in.
   public function getMyContactsList($user_id)
   {
      $result = $this->db->query ( 
            "select CONTACT_ID, SERVICE FROM CONTACT WHERE UID = " . $user_id
            );
   
      if (! $result)
      {
         return "Error: " . $this->db->error;
      }

      $i = 0;
      $ret = array();
      while ( $row = $result->fetch_assoc () )
      {
         array_push($ret, array (
                                 'contact_id' => $row ['CONTACT_ID'],
                                 'service' => $row ['SERVICE']
                        ));
      }
      
      mysqli_free_result($result);
      
      return Json_encode($ret);
   }
   
   public function updateMyLastAccessDate($user_id)
   {
      $result = $this->db->query (
               "UPDATE `USER` SET " .
               "LAST_ACCESS_DATE='" . date ( 'Y-m-d H:i:s' ) . "' " .
               "WHERE UID = " . $user_id );
      if (! $result)
      {
         return "Error " . $this->db->error;
      }
      
      if ($this->db->commit ())
      {
         return "OK";
      }
   }
}

?>