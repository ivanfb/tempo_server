
<?php

require_once 'library/Log.php';
require_once 'library/Config.php';
require_once 'library/Shared.php';

$json = file_get_contents ( 'php://input' );
$obj = json_decode ( $json );
$user_id = $obj->{'user_id'};
$contacts = $obj->{'contacts'};

try {
	echo DB::getInstance()->deleteContacts($user_id, $contacts);
} catch ( Exception $e ) {
	die ( 'Error in removeContacts : ' . $e->getMessage () );
	echo "ERROR " . $e->getMessage ();
}

?>