
<?php

require_once 'library/Log.php';
require_once 'library/Config.php';
require_once 'library/Shared.php';

$json = file_get_contents ( 'php://input' );
$obj = json_decode ( $json );
$user_id = $obj->{'user_id'};
$token = $obj->{'token'};

try {
	echo DB::getInstance()->saveFirebaseToken($user_id, $token);
} catch ( Exception $e ) {
	die ( 'Error in saveFirebaseToken : ' . $e->getMessage () );
	echo "ERROR " . $e->getMessage ();
}

?>
