
<?php

require_once 'library/Log.php';
require_once 'library/Config.php';
require_once 'library/Shared.php';

$json = file_get_contents ( 'php://input' );
$obj = json_decode ( $json );
$user_id = $obj->{'user_id'};
$message = $obj->{'message'};

try {
   
   echo DB::getInstance()->addUserError($user_id, $message);

} catch ( Exception $e ) {
	die ( 'Error in saveUserInfo : ' . $e->getMessage () );
	echo "ERROR " . $e->getMessage ();
}

?>