
<?php

require_once 'library/Log.php';
require_once 'library/Config.php';
require_once 'library/Shared.php';

$json = file_get_contents ( 'php://input' );
$obj = json_decode ( $json );
$user_id = $obj->{'user_id'};
$user_name = $obj->{'user_name'};
$location = $obj->{'location'};
$lon = $obj->{'lon'};
$lat = $obj->{'lat'};
$services = $obj->{'services'};

try {
	echo DB::getInstance()->updateUserInfo($user_id, 
										   $user_name,
										   $location,
										   $lon,
										   $lat,
			                        $services);
} catch ( Exception $e ) {
	die ( 'Error in saveUserInfo : ' . $e->getMessage () );
	echo "ERROR " . $e->getMessage ();
}

?>