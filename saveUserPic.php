
<?php

require_once 'library/Log.php';
require_once 'library/Config.php';
require_once 'library/Shared.php';

// The "user pays" og GCP Bitbucket gave a lot of problems. It should be disabled.
// To see its status --> gsutil requesterpays get gs://staging.tempo-213421.appspot.com
// To enable/disable it --> gsutil requesterpays set off gs://staging.tempo-213421.appspot.com

$target_dir = 'users_pics/';
// Check if image file is a actual image or fake image
if (is_uploaded_file($_FILES['my_pic_file']['tmp_name']) &&
		isset ($_POST['file_name'])) {
	$tmp_name = $_FILES['my_pic_file']['tmp_name'];
	$pic_name = $_POST['file_name'];

	$target_file = ${target_dir}.${pic_name}.'.jpeg';

	// Disabling cache so that users' pics can be updated rapidly to other users.
	$options = ['gs' => ['Cache-Control' => 'private, max-age=0, no-transform',
	                     'Content-type' => 'image/jpeg']];
	$context = stream_context_create($options);

	$data = $html = file_get_contents($tmp_name);

	// TODO: extract the bucket name from configuration file
   if (file_put_contents('gs://staging.tempo-213421.appspot.com/'.$target_file, $data, 0, $context))
	{
      DB::getInstance()->updatePicChangeTime($_POST['user_id']);
 	   echo "OK " . $tmp_name . "||" . $target_file;
	}
	else {
	   echo "ERROR in saveUserPic.php:move_uploaded_file " . error_get_last()['message'];
	}
}
else{
	echo "File not uploaded successfully.";
}
?>
