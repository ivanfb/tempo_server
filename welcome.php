<?php 

/**
 * Redirect with POST data.
 *
 * @param string $url URL.
 * @param array $post_data POST data. Example: array('foo' => 'var', 'id' => 123)
 * @param array $headers Optional. Extra headers to send.
 */
function redirect_post($url, array $data, array $headers = null) {
   $params = array(
            'http' => array(
                     'method' => 'POST',
                     'content' => http_build_query($data)
            )
   );
   if (!is_null($headers)) {
      $params['http']['header'] = '';
      foreach ($headers as $k => $v) {
         $params['http']['header'] .= "$k: $v\n";
      }
   }
   $ctx = stream_context_create($params);
   $fp = @fopen($url, 'rb', false, $ctx);
   if ($fp) {
      echo @stream_get_contents($fp);
      die();
   } else {
      // Error
      throw new Exception("Error loading '$url', $php_errormsg");
   }
}

function encrypt_decrypt($action, $string) {
   $output = false;
   $encrypt_method = "AES-128-ECB";
   
   $key = base64_decode("AKgBExWb4EPuuSzAvsPfng==");
   
   if ( $action == 'encrypt' ) {
      $output = openssl_encrypt($string, $encrypt_method, $key, OPENSSL_RAW_DATA);
      $output = base64_encode($output);
   } else if( $action == 'decrypt' ) {
      $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, OPENSSL_RAW_DATA);
   }
   return $output;
}

// the decrypt part, extracted from UserController.php
$encrypt_method = "AES-128-ECB";
$key = base64_decode("AKgBExWb4EPuuSzAvsPfng==");

$encrypted_uid = rawurldecode($_GET['a']);

$uid = encrypt_decrypt('decrypt', $encrypted_uid);

redirect_post("https://tempo-213421.appspot.com/index.php", 
         array('action' => 'confirm_email_account', 
               'controller' => 'User',
               'uid' => $uid
         ));
?>


